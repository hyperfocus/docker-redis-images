REG_ADDRESS=registry.gitlab.com
USERNAME=supernami
VERSION=1.0

redis-build:
	docker-compose -f redis.yml build

redis-tag:
	docker tag redis:latest ${REG_ADDRESS}/${USERNAME}/docker-redis-images:base-${VERSION} && \
	docker tag ${REG_ADDRESS}/${USERNAME}/docker-redis-images:base-${VERSION} ${REG_ADDRESS}/${USERNAME}/docker-redis-images:base-latest

redis-push:
	docker push ${REG_ADDRESS}/${USERNAME}/docker-redis-images:base-${VERSION} && \
	docker push ${REG_ADDRESS}/${USERNAME}/docker-redis-images:base-latest
